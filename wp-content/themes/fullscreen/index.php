<?php
/**
 * The main template file.
 */
?>

<?php get_header(); ?>

<?php if (have_posts()) {
    while (have_posts()) {
        the_post();
        ?>
        <div id="fullpage">

            <?php get_template_part('template-parts/content', 'intro'); ?>

            <?php get_template_part('template-parts/content', 'section'); ?>

        </div>
        <?php
    }
} else {

}?>

<?php get_footer(); ?>
