<?php
/**
 * Templates functions.
 */

/**
 * @param $btn_text
 */
function fs_display_btn($btn_text) {
    ?>
    <div class="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-xs-8 offset-xs-2">
        <button type="button" class="btn btn-block btn-primary"><?= $btn_text ?></button>
    </div>
    <?php
}

/**
 * @param $btn_text_block
 */
function fs_display_btn_block($text, $url, $type, $pre_text, $sub_text) {
    ?>
    <div class="col-md-4 offset-md-4 col-sm-6 offset-sm-3 col-xs-12">
        <?php
        if ($pre_text) {
            ?>
            <span class="remark hidden-xs-down"><?= $pre_text ?></span>
            <?php
        }
        ?>

        <a href="<?= $url ?>" class="btn btn-block btn-<?= $type ?>"><?= $text ?></a>

        <?php
        if ($sub_text) {
            ?>
            <span class="remark hidden-xs-down"><?= $sub_text ?></span>
            <?php
        }
        ?>
    </div>
    <?php
}

function fs_header() {
    ?>
    <header id="header" class="hidden-xs-down">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-3">
                    <div id="logo-container-sm">
                        <img class="hidden-xs-down" src="<?= BR_IMG . '/logo.png' ?>" alt="">
                    </div>
                </div>
                <div class="col-md-6 col-sm-9">
                    <?php
                    if(has_nav_menu('primary')) {
                        ?>
                        <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'primary',
                                'menu_class'     => 'primary-menu',
                            ) );
                            ?>
                        </nav>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
        <div class="border"></div>
    </header>
    <?php
}

function fs_float_header() {
    ?>
    <div id="header-float" class="hidden-sm-up">
        <div class="container">
            <div class="row">
                <div class="col-xs-3">
                    <div id="logo-container-mobile">
                        <img src="<?= BR_IMG . '/logo.png' ?>" alt="">
                    </div>
                </div>
                <div class="col-xs-9">
                    <?php
                    if(has_nav_menu('primary')) {
                        ?>
                        <nav id="site-navigation" class="main-navigation" role="navigation" aria-label="<?php esc_attr_e( 'Primary Menu', 'twentysixteen' ); ?>">
                            <?php
                            wp_nav_menu( array(
                                'theme_location' => 'primary',
                                'menu_class'     => 'primary-menu',
                            ) );
                            ?>
                        </nav>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>
    </div>
    <?php
}