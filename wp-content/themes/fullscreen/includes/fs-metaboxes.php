<?php
$btn_type_options = array (
    'primary' => __('Primary', 'fullscreen'),
    'secondary' => __('Secondary', 'fullscreen'),
    'success' => __('Success', 'fullscreen'),
    'info' => __('Info', 'fullscreen'),
    'warning' => __('Warning', 'fullscreen'),
    'danger' => __('Danger', 'fullscreen'),
    'link' => __('Link', 'fullscreen'),
);

$atts = array(
    'id' => 'demo_meta',
    'title' => __('Full screen sections', 'domain'),
    'object_types' => array('page')
);
$fields = array(
    // Intro section
    array(
        'type' => 'title',
        'id' => 'intro_section_title',
        'name' => __('Intro section', 'fullscreen')
    ),
    array(
        'type' => 'checkbox',
        'id' => 'is_content_intro_checkbox',
        'name' => __('Use content as Intro section', 'fullscreen'),
        'desc' => __('Check this if you want to use content field as intro section for this page', 'fullscreen')
    ),
    array (
        'type' => 'select',
        'id' => 'intro_button_type_select',
        'name' => __('Intro button type', 'fullscreen'),
        'desc'=> __('Select button type', 'fullscreen'),
        'default' => 'primary',
        'options' => $btn_type_options,
    ),
    array(
        'type' => 'text',
        'name' => __('Intro button text', 'fullscreen'),
        'id' => 'intro_button_text'
    ),
    array(
        'type' => 'text_url',
        'name' => __('Intro button URL', 'fullscreen'),
        'id' => 'intro_button_text_url'
    ),
    array(
        'type' => 'text',
        'name' => __('Intro button pretext', 'fullscreen'),
        'id' => 'intro_button_pre_text'
    ),
    array(
        'type' => 'text',
        'name' => __('Intro button subtext', 'fullscreen'),
        'id' => 'intro_button_sub_text'
    ),

    // Sections Group
    array(
        'type' => 'group',
        'id' => 'repeat_sections_group',
        'name' => 'Fullscreen sections',
        'description' => __('You can add new, edit or remove existing fullscreen', 'fullscreen'),

        'options' => array(
            'group_title'   => __('Section {#}', 'fullscreen'),
            'add_button'    => __('Add Another Section', 'fullscreen'),
            'remove_button' => __('Remove Section', 'fullscreen'),
            'sortable'      => true,
        ),
        'group_fields' => array(
            // section header
            array (
                'type' => 'text',
                'id'   => 'section_id_text',
                'name' => __('Section ID', 'fullscreen'),
            ),
            array (
                'type' => 'file',
                'id' => 'section_thumb_file',
                'name' => __('Section Header thumbnail', 'fullscreen'),
                'desc'  => __('Upload an image or enter an URL.', 'fullscreen'),
            ),
            array (
                'type' => 'text',
                'id'   => 'section_title_text',
                'name' => __('Section Title', 'fullscreen'),
                'sanitization_cb' => false
            ),
            array (
                'type' => 'text',
                'id'   => 'section_pre_title_text',
                'name' => __('Section Pretitle', 'fullscreen'),
                'description' => __('Input pretitle text, which would be displayed above Section title', 'fullscreen'),
            ),
            array (
                'type' => 'text',
                'id'   => 'section_sub_title_text',
                'name' => __('Section Subtitle', 'fullscreen'),
                'description' => __('Input subtitle text, which would be displayed under Section title', 'fullscreen'),
            ),

            // section content
            array (
                'type' => 'wysiwyg',
                'id' => 'section_content_wysiwyg',
                'name' => __('Section Content', 'fullscreen'),
                'sanitization_cb' => false
            ),

            // Section button
            array (
                'type' => 'select',
                'id' => 'section_button_type_select',
                'name' => __('Intro button type', 'fullscreen'),
                'desc'=> __('Select button type', 'fullscreen'),
                'default' => 'primary',
                'options' => $btn_type_options,
            ),
            array(
                'type' => 'text',
                'name' => __('Intro button text', 'fullscreen'),
                'id' => 'section_button_text'
            ),
            array(
                'type' => 'text_url',
                'name' => __('Intro button URL', 'fullscreen'),
                'id' => 'section_button_text_url'
            ),
            array(
                'type' => 'text',
                'name' => __('Intro button pretext', 'fullscreen'),
                'id' => 'section_button_pre_text'
            ),
            array(
                'type' => 'text',
                'name' => __('Intro button subtext', 'fullscreen'),
                'id' => 'section_button_sub_text'
            ),
        ),
    )
);

$cmb2 = new SNTH_FW_CMB2_Metaboxes($atts, $fields, 'fs');