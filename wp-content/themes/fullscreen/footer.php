<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package beetroot
 */

?>

            <footer class="hidden-xs-down">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-lg-7 col-md-6 col-sm-12 scroll-bar">
                            <div id="slider-container" class="scroll-progress-container">
                                <span id="autoplay" href="#" class="icon run-ico"> </span>
                                <div id="section-slider"></div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-6 col-sm-12 icons">
                            <div class="icons-container">
                                <div class="action-ico">
                                    <span href="#" class="icon grayscale-ico"> </span>
                                    <span href="#" id="fsbutton" class="icon fullscreen-ico"> </span>
                                </div>
                                <div class="social-ico">
                                    <a href="http://#" class="icon facebook-ico"> </a>
                                    <a href="http://#" class="icon twitter-ico"> </a>
                                    <a href="http://#" class="icon linkedin-ico"> </a>
                                </div>
                            </div>
                            <div class="design-by-container">
                                <span class="">Design by Tross </span><img src="<?= BR_IMG . '/tross.png' ?>" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </footer>
        </div><!-- #page -->
        <?php wp_footer(); ?>
    </body>
</html>