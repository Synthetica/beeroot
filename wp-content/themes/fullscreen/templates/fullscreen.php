<?php
/**
 * Template Name: Full Screen Sections Template
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package beetroot
 */
?>

<?php get_header(); ?>

<div id="fullpage">
    <div id="intro" class="section">

        <?php fs_float_header() ?>

        <div class="container-fluid section-container">
            <div id="logo-container-lg" class="row">
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <img class="hidden-xs-down" src="<?= BR_IMG . '/logo.png' ?>" alt="">
                </div>
            </div>

            <header>
                <div class="row">
                    <div class="col-md-8 offset-md-2">
                        <h3 class="pre-header"><?= __('Better viewership is', 'fullscreen') ?></h3>
                        <h1><?= __('Better ROI', 'fullscreen') ?></h1>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row">
                    <div class="col-md-4 featured-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/film.png' ?>" alt="">
                        </div>
                        <p class="text-center text-xs-center"><?= __('A brand new video player', 'fullscreen') ?></p>
                    </div>

                    <div class="col-md-4 featured-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/cogs.png' ?>" alt="">
                        </div>
                        <p class="text-center text-xs-center"><?= __('Advanced yield optimization', 'fullscreen') ?></p>
                    </div>

                    <div class="col-md-4 featured-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/map.png' ?>" alt="">
                        </div>
                        <p class="text-center text-xs-center"><?= __('Personal monetization strategy', 'fullscreen') ?></p>
                    </div>
                </div>
            </div><!-- .section-content -->

            <div class="row">
                <?php fs_display_btn(__('CONTACT US', 'fullscreen')) ?>
            </div>
        </div>
    </div>

    <div id="promo" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/fly.png' ?>" alt="">
                        </div>
                        <h1 class="text-center text-xs-center">THE END OF ANNOYING VIDEO ADS.</h1>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-1">
                        <p>
                            Our algorythms learn audiences' behaviors. It then shows them ads they're more likely to want to see. Coupled with our awesome video ad
                            <a href="#">units</a> and powerful yield management, we're bringing back a time  when ads were effective.
                        </p>
                    </div>
                </div>
            </div><!-- .section-content -->

            <div class="row">
                <?php fs_display_btn(__('GET IN TOUCH', 'fullscreen')) ?>
            </div>
        </div>
    </div>

    <div id="technology" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/leptop.png' ?>" alt="">
                        </div>
                        <h1 class="text-xs-center"><?= __('THE TECHNOLOGY', 'fullscreen') ?></h1>
                        <h2 class="sub-header text-xs-center"><?= __('NOT JUST BUZZWORDS, THEY\'RE THE ADVANTAGES', 'fullscreen') ?></h2>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-1">
                        <div class="inline-list">
                            <div class="list-item">
                                <div class="num">01</div>
                                <?= __('Cross Platform - Our player and technology are designed to be rock solid on any platform, no hiccups.', 'fullscreen') ?>
                            </div>
                            <div class="list-item">
                                <div class="num">02</div>
                                <?= __('Our ROI boosting ad ranking engine takes both user engagement and advertise bids into account.', 'fullscreen') ?>
                            </div>
                            <div class="list-item">
                                <div class="num">03</div>
                                <?= __('Predictive Placement - Our platform displays the optimal ad for the right user in the most effective scree placement for them.', 'fullscreen') ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- .section-content -->

            <div class="row">
                <?php fs_display_btn(__('READ MORE', 'fullscreen')) ?>
            </div>
        </div>
    </div>

    <div id="howto" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/bulb.png' ?>" alt="">
                        </div>
                        <h1 class="text-xs-center"><?= __('HOW IT WORKS?', 'fullscreen') ?></h1>
                        <h2 class="sub-header text-xs-center"><?= __('MAGIC! OK NOT REALLY', 'fullscreen') ?></h2>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row">
                    <div class="col-lg-6 offset-lg-3 col-md-8 offset-md-2 col-sm-10 offset-sm-1">
                        <p>
                            <?= __('We bealieve that ad revenue can be improved. Not by increasing the amount of ads displayed? but by using big data to deliver the right ads tp the right viewers in the right way.', 'fullscreen') ?>
                        </p>

                        <p>
                            <?= __('We\'re also combating industry tricks that don\'t work anymore anyway. That\'s why we\'re using our own ad curation system instead of relying on advertisers who often miscategorize their ads to gain more views.', 'fullscreen') ?>
                        </p>
                    </div>
                </div>
            </div><!-- .section-content -->
        </div>
    </div>

    <div id="benefits" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/like.png' ?>" alt="">
                        </div>
                        <h1 class="text-center text-xs-center"><?= __('BENEFITS', 'fullscreen') ?></h1>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row block-list">
                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">01</div>
                        <h2 class="sub-header">
                            <?= __('Ads Your Audience Won\'t Hate', 'fullscreen') ?></h2>
                        <p><?= __('We focus on your viewers, individually. By displaying ads they\'re likely to be interested in and not showing ads that increase bounce rates.', 'fullscreen') ?></p>
                    </div>

                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">02</div>
                        <h2 class="sub-header"><?= __('Reducing Data Usage', 'fullscreen') ?></h2>
                        <p><?= __('Too much of user\'s data plan is spent o ads. Our compression significantly minimizes that, enabling them to spend more time on your site.', 'fullscreen') ?></p>
                    </div>

                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">03</div>
                        <h2 class="sub-header"><?= __('Custom Monetization strategy', 'fullscreen') ?></h2>
                        <p><?= __('Users are targeted based on their interests and shown ads they like. Our players measures how long they watched ads? if they click? and more.', 'fullscreen') ?></p>
                    </div>

                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">04</div>
                        <h2 class="sub-header"><?= __('Taylored For Every Publisher', 'fullscreen') ?></h2>
                        <p><?= __('We customize our player and ads to your specific needs? both in content and the way it appears.', 'fullscreen') ?></p>
                    </div>

                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">05</div>
                        <h2 class="sub-header"><?= __('We Care About Our Publishers', 'fullscreen') ?></h2>
                        <p><?= __('And about their bottom line. It\'s not about playing the ads that cost a bit more? it\'s about playing the ones that\'ll get the most total revenue. It\'s an important distinction.', 'fullscreen') ?></p>
                    </div>

                    <div class="col-lg-4 col-md-6 list-item">
                        <div class="num">06</div>
                        <h2 class="sub-header"><?= __('Maximum Yield, Minimum Disruption', 'fullscreen') ?></h2>
                        <p><?= __('Getting the best numbers with minimum distractions to the user. Increasing engagement while lowering bounce rates.', 'fullscreen') ?></p>
                    </div>
                </div>
            </div><!-- .section-content -->
        </div>
    </div>

    <div id="testimonials" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <h1 class="text-center text-xs-center">WE <img src="<?= BR_IMG . '/heart.png' ?>" alt=""> PUBLISHERS</h1>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <div class="row">
                    <div class="col-md-4 testimonials-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/male.png' ?>" alt="">
                        </div>
                        <p>"I'm embarrassed to admit we tried 4 or 5 other ways to monetize our 2M monthly visitors. Your video player made it so easy."</p>
                    </div>

                    <div class="col-md-4 testimonials-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/malecostume.png' ?>" alt="">
                        </div>
                        <p>"By putting our users experience first? we were able to boost revenue by 2.5x, who knew it was all about the viewers."</p>
                    </div>

                    <div class="col-md-4 testimonials-box">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/matureman.png' ?>" alt="">
                        </div>
                        <p>"We've gotten nothing but positive feedback from our users? both in emails and click through rates. Thanks!"</p>
                    </div>
                </div>
            </div><!-- .section-content -->

            <div class="row">
                <?php fs_display_btn(__('GET IN TOUCH', 'fullscreen')) ?>
            </div>
        </div>
    </div><!-- #testimonials -->

    <div id="feedback" class="section">
        <div class="container-fluid section-container">
            <header>
                <div class="row">
                    <div class="col-xs-10 offset-xs-1">
                        <div class="ico-container">
                            <img src="<?= BR_IMG . '/drop.png' ?>" alt="">
                        </div>
                        <h1 class="text-center text-xs-center">DROP US A LINE</h1>
                    </div>
                </div>
            </header><!-- .section-header -->

            <div class="section-content">
                <form action="">
                    <div class="row">
                        <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                            <div class="row form-input-container">
                                <div class="col-sm-6">
                                    <div class="form-group pseudo-table">
                                        <input id="feedback_name" type="text" class="form-control" placeholder="Your Name">
                                        <label for="feedback_name" class="hidden-xs-down">Name</label>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group pseudo-table">
                                        <input id="feedback_company" type="text" class="form-control" placeholder="Your Company Name">
                                        <label for="feedback_company" class="hidden-xs-down">Company Name</label>
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input id="feedback_email" type="email" class="form-control" placeholder="Email Address">
                                    </div>
                                </div>

                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <input id="feedback_company_activity" type="text" class="form-control" placeholder="What does your company do?">
                                    </div>
                                </div>

                                <div class="col-sm-12">
                                    <div class="form-group">
                                        <input id="feedback_message" type="text" class="form-control" placeholder="Message">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-4 offset-sm-4">
                            <button type="button" class="btn btn-block btn-primary" style="margin-bottom:10px"><?= __('SEND', 'fullscreen') ?></button>
                            <p class="remark hidden-xs-down">We usually respond in less then 24 hours</p>
                        </div>
                    </div>
                </form>
            </div><!-- .section-content -->

        </div>
        <div id="footer-float" class="hidden-sm-up">Footer</div>
    </div>
</div>

<?php get_footer(); ?>

