<!-- Intro section - Start -->
<?php
if (get_post_meta( get_the_ID(), '_fs_is_content_intro_checkbox', 0)) {

    ?>
    <div id="intro" class="section">

        <?php fs_float_header() ?>

        <div class="container-fluid section-container">
            <div id="logo-container-lg" class="row">
                <div class="col-lg-8 offset-lg-2 col-md-10 offset-md-1">
                    <img class="hidden-xs-down" src="<?= BR_IMG . '/logo.png' ?>" alt="">
                </div>
            </div>

            <?php the_content() ?>

            <?php
            if(get_post_meta( get_the_ID(), '_fs_intro_button_text')) {
                $btn_text = get_post_meta(get_the_ID(), '_fs_intro_button_text', true);
                $btn_url = get_post_meta(get_the_ID(), '_fs_intro_button_text_url', true);
                $btn_type = get_post_meta(get_the_ID(), '_fs_intro_button_type_select', true);
                $btn_pre_text = get_post_meta(get_the_ID(), '_fs_intro_button_pre_text', true);
                $btn_sub_text = get_post_meta(get_the_ID(), '_fs_intro_button_sub_text', true);
                ?>

                <div class="row">
                    <?php fs_display_btn_block($btn_text, $btn_url, $btn_type, $btn_pre_text, $btn_sub_text) ?>
                </div>

                <?php
            }
            ?>
        </div>
    </div>
    <?php
}
?>
<!-- Intro section - End -->