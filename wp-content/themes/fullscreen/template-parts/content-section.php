
<!-- Other sections - Start -->
<?php
if(get_post_meta( get_the_ID(), '_fs_repeat_sections_group')) {
    $sections = get_post_meta( get_the_ID(), '_fs_repeat_sections_group', true);
    $count = count($sections);
    $counter = 0;
    foreach ($sections as $section) {
        ++$counter;
        ?>
        <div id="<?= $section['section_id_text'] ?>" class="section">
            <div class="container-fluid section-container">
                <div class="section-container-inner">
                    <!-- Section Header - Start -->
                    <header>
                        <div class="row">
                            <div class="col-xs-10 offset-xs-1">
                                <?php
                                if($section['section_thumb_file']) {
                                    ?>
                                    <div class="ico-container">
                                        <img src="<?= $section['section_thumb_file'] ?>" alt="">
                                    </div>
                                    <?php
                                }
                                ?>
    
                                <?php
                                if ($section['section_pre_title_text']) {
                                    ?><h2 class="pre-header text-xs-center"><?= $section['section_pre_title_text'] ?></h2><?php
                                }
                                ?>
    
                                <h1 class="text-center text-xs-center"><?= $section['section_title_text'] ?></h1>
    
                                <?php
                                if ($section['section_sub_title_text']) {
                                    ?><h2 class="sub-header text-xs-center"><?= $section['section_sub_title_text'] ?></h2><?php
                                }
                                ?>
                            </div>
                        </div>
                    </header>
                    <!-- Section Header - End -->
    
                    <!-- Content - Start -->
                    <?php
                    if($section['section_content_wysiwyg']) {
                        ?>
                        <div class="section-content">
                            <?= do_shortcode($section['section_content_wysiwyg']); ?>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- Content - End -->
    
                    <!-- Button block - Start -->
                    <?php
                    if($section['section_button_text']) {
                        $btn_text = $section['section_button_text'];
                        $btn_url = $section['section_button_text_url'];
                        $btn_type = $section['section_button_type_select'];
                        $btn_pre_text = $section['section_button_pre_text'];
                        $btn_sub_text = $section['section_button_sub_text'];
                        ?>
                        <div class="row">
                            <?php fs_display_btn_block($btn_text, $btn_url, $btn_type, $btn_pre_text, $btn_sub_text) ?>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- Button block - End -->
                </div><!-- .section-container-inner -->
            </div><!-- .container-fluid.section-container -->
            
            <?php
            if ($count == $counter) {
                ?>
                <div id="footer-float" class="hidden-sm-up">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-xs-12 scroll-bar">
                                <div id="slider-container" class="scroll-progress-container">
                                    <a id="autoplay-mobile" href="#" class="icon totop-ico"> </a>
                                    <div id="section-slider-mobile"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 mobile-icons">
                                <div class="design-by-container">
                                    Design by Tross <img src="<?= BR_IMG . '/tross.png' ?>" alt="">
                                </div>
                                <div class="mobile-icons-container">
                                    <div class="social-ico">
                                        <a href="http://#" class="mobile-icon facebook-mobile-ico"> </a>
                                        <a href="http://#" class="mobile-icon twitter-mobile-ico"> </a>
                                        <a href="http://#" class="mobile-icon linkedin-mobile-ico"> </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
            }
            ?>
        </div>
        <?php
    }
}
?>
<!-- Other sections - End -->