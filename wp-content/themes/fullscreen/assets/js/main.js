(function($) {
    var interval;
    var fullpafe = $('#fullpage');
    var autoplayBtn = $('#autoplay');
    var autoplayTime = 3000;
    var sectionsNum = $('#fullpage .section').length;
    var sectionSlider = document.getElementById('section-slider');
    var sectionSliderMobile = document.getElementById('section-slider-mobile');

    $(document).ready(function() {
        init_noUIslider();
        init_fullpage();
        init_fullscreen();
        init_autoscroll();
        init_scrolltotop();
        update_noUIslider();
        init_grayscale();
    });

    /* ----------------------
        01 - FULLPAGE
       ---------------------- */
    function init_fullpage() {
        //big_screen_fullpage();
        if ($(window).width() < 576) {
            small_screen_fullpage();
        } else {
            big_screen_fullpage();
        }
    }

    function big_screen_fullpage() {
        fullpafe.fullpage({
            scrollOverflow: true,

            onLeave: function(index, nextIndex, direction) {
                if (index == 1 && direction =='down') {
                    setTimeout(small_logo_display, 500);
                }
                if (index == sectionsNum) {
                    autoplayBtn.removeClass('totop-ico').removeClass('stop-ico').addClass('run-ico');
                    $('.design-by-container').fadeOut(800);
                }
            },
            afterLoad: function(anchorLink, index) {
                sectionSlider.noUiSlider.set([index]);
                if (index == 1) {
                    small_logo_hide();
                }
                if (index == sectionsNum) {
                    autoplayBtn.removeClass('stop-ico').removeClass('run-ico').addClass('totop-ico');
                    clearInterval(interval);
                    $('.design-by-container').fadeIn(800);
                }
            }
        });
    }

    function small_screen_fullpage() {
        fullpafe.fullpage({
            responsiveWidth: 576,

            onLeave: function(index, nextIndex, direction) {
                if (index == 1 && direction =='down') {
                    setTimeout(small_logo_display, 500);
                }
            },
            afterLoad: function(anchorLink, index) {
                if (index == 1) {
                    small_logo_hide();
                }
            }
        });
    }

    /* ----------------------
         02 - GRAYSCALE
       ---------------------- */
    function init_grayscale() {
        $('.grayscale-ico').toggle(function(){
            grayscale($('#page'));
        }, function(){
            grayscale.reset($('#page'));
        });
    }

    /* ----------------------
         03 - GRAYSCALE
       ---------------------- */
    function init_fullscreen() {
        var fsButton = document.getElementById('fsbutton'),
            //fsElement = document.getElementById('page');
            fsElement = document.getElementsByTagName('body')[0];

        if (window.fullScreenApi.supportsFullScreen) {

            // handle button click
            fsButton.addEventListener('click', function() {
                window.fullScreenApi.requestFullScreen(fsElement);
            }, true);

            fsElement.addEventListener(fullScreenApi.fullScreenEventName, function() {
                if (fullScreenApi.isFullScreen()) {

                } else {

                }
            }, true);

        } else {

        }
    }

    /* ----------------------
         04 - LOGO WITH SCROLL
       ---------------------- */
    function small_logo_display() {
        $('#logo-container-sm  img').animate({
            top: '16px'
        }, 500);
    }
    function small_logo_hide() {
        $('#logo-container-sm  img').animate({
            top: '-200px'
        }, 500);
    }

    /* ----------------------
     05 - SECTIONS AUTOSCROLL
     ---------------------- */
    function init_autoscroll() {
        $('#autoplay').click(function() {
            if($(this).hasClass('run-ico')) {
                $(this).removeClass('run-ico').addClass('stop-ico');
                interval = setInterval( function() {
                    $.fn.fullpage.moveSectionDown();
                }, autoplayTime);
            } else if ($(this).hasClass('totop-ico')) {
                $(this).removeClass('totop-ico').removeClass('stop-ico').addClass('run-ico');
                $.fn.fullpage.moveTo(1);
            } else {
                $(this).removeClass('stop-ico').addClass('run-ico');
                clearInterval(interval);
            }
        });
    }

    /* ----------------------
     06 - SCROLL TO TOP
     ---------------------- */
    function init_scrolltotop() {
        $('#autoplay-mobile').click(function() {
            $('html, body').animate({scrollTop : 0},800);
            return false;
        });
    }

    function init_noUIslider() {
        noUiSlider.create(sectionSlider, {
            start: [1],
            step: 1,
            connect: [true, false],
            range: {
                'min': [ 1],
                'max': [sectionsNum]
            }
        });

        noUiSlider.create(sectionSliderMobile, {
            start: [1],
            step: 1,
            connect: [true, false],
            range: {
                'min': [ 1],
                'max': [sectionsNum]
            }
        });
    }

    function update_noUIslider() {
        sectionSlider.noUiSlider.on('change', function( values, handle ) {
            $.fn.fullpage.moveTo(Math.round(values));
        });
    }

})(jQuery);