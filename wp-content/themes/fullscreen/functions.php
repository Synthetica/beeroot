<?php
/** -----------------------------------------
 *    01. Define constants
 *  ----------------------------------------- */
define('BR_VERSION', '1.0.0');

define('BR_PATH', get_stylesheet_directory());
define('BR_PARENT_PATH', get_template_directory());
define('BR_INC', BR_PATH . '/includes');

define('BR_URI', get_stylesheet_directory_uri());
define('BR_PARENT_URI', get_template_directory_uri());
define('BR_ASSETS', BR_URI . '/assets');
define('BR_CSS', BR_ASSETS . '/css');
define('BR_JS', BR_ASSETS . '/js');
define('BR_IMG', BR_ASSETS . '/img');
define('BR_VENDORS', BR_ASSETS . '/vendors');

/** -----------------------------------------
 *    02. Theme setup
 *  ----------------------------------------- */
if (!function_exists('br_theme_setup')) {
    function br_theme_setup()
    {
        load_theme_textdomain( 'fullscreen', get_template_directory() . '/languages' );

        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support('post-thumbnails');
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));
        add_theme_support('custom-background', apply_filters( 'beetroot_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        register_nav_menu('primary', __('Header navigation menu', 'fullscreen'));
    }
}

add_action('after_setup_theme', 'br_theme_setup');

/** -----------------------------------------
 *    03. Styles enqueing
 *  ----------------------------------------- */
if(!function_exists('br_theme_enqueue_styles')) {
    function br_theme_enqueue_styles()
    {
        wp_register_style('robotoFont', 'https://fonts.googleapis.com/css?family=Roboto:400,500,700,900');
        wp_enqueue_style('robotoFont');

        wp_register_style('tether', BR_VENDORS . '/tether/css/tether.css', array(), BR_VERSION);
        wp_enqueue_style('tether');

        // BS 4
        wp_register_style('bootstrap', BR_VENDORS . '/bootstrap4/dist/css/bootstrap.css', array(), BR_VERSION);
        //wp_register_style('bootstrap', BR_VENDORS . '/bootstrap/dist/css/bootstrap.min.css');
        wp_enqueue_style('bootstrap');

        wp_register_style('fullpage', BR_VENDORS . '/fullPage/jquery.fullPage.css', array(), BR_VERSION);
        wp_enqueue_style('fullpage');

        wp_register_style('nouislider', BR_VENDORS . '/noUiSlider/nouislider.css', array(), BR_VERSION);
        wp_enqueue_style('nouislider');

        wp_register_style('style', BR_CSS . '/main.css', array('bootstrap'), BR_VERSION);
        wp_enqueue_style('style');
    }
}
add_action('wp_enqueue_scripts', 'br_theme_enqueue_styles');

/** -----------------------------------------
 *    04. Scripts enqueing
 *  ----------------------------------------- */
if(!function_exists('br_theme_enqueue_scripts')) {
    function br_theme_enqueue_scripts()
    {
        wp_register_script('tether', BR_VENDORS . '/tether/js/tether.js', array('jquery'), BR_VERSION, true);
        wp_enqueue_script('tether');

        // BS 4
        wp_register_script('bootstrap', BR_VENDORS . '/bootstrap4/dist/js/bootstrap.js', array('jquery', 'tether'), BR_VERSION, true);
        //wp_register_script('bootstrap', BR_VENDORS . '/bootstrap/dist/js/bootstrap.min.js');
        wp_enqueue_script('bootstrap');

        wp_register_script('scrolloverflow', BR_VENDORS . '/fullPage/vendors/scrolloverflow.min.js', array('jquery'), BR_VERSION, true);
        wp_enqueue_script('scrolloverflow');
        wp_register_script('fullpage', BR_VENDORS . '/fullPage/jquery.fullPage.js', array('jquery'), BR_VERSION, true);
        wp_enqueue_script('fullpage');

        wp_register_script('nouislider', BR_VENDORS . '/noUiSlider/nouislider.js', array(), BR_VERSION, true);
        wp_enqueue_script('nouislider');

        wp_register_script('grayscale', BR_VENDORS . '/grayscale/grayscale.js', array(), BR_VERSION, true);
        wp_enqueue_script('grayscale');

        wp_register_script('fullscreen', BR_VENDORS . '/fullScreenApi/full-screen-api.js', array(), BR_VERSION, true);
        wp_enqueue_script('fullscreen');

        wp_register_script('script', BR_JS . '/main.js', array('bootstrap', 'fullpage'), BR_VERSION, true);
        wp_enqueue_script('script');
    }
}
add_action('wp_enqueue_scripts', 'br_theme_enqueue_scripts');

/** -----------------------------------------
 *    05. Including files
 *  ----------------------------------------- */
if (file_exists(BR_INC . '/fs-templates.php')) {
    require_once BR_INC . '/fs-templates.php';
}
if (file_exists(BR_INC . '/fs-metaboxes.php')) {
    require_once BR_INC . '/fs-metaboxes.php';
}
