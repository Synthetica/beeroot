<?php

class SNTH_FW_CMB2_Metaboxes
{
    protected $settings = array();

    protected $prefix = '';

    protected $atts;
    protected $fields;
    protected $metabox;

    /**
     * SNTH_FW_CMB2_Metaboxes constructor.
     *
     * @param $atts array (require: 'id', 'title', 'object_types')
     * @param $fields array(require: 'id', 'type')
     * @link https://github.com/WebDevStudios/CMB2/blob/master/example-functions.php
     */
    public function __construct($atts, $fields, $prefix = false)
    {
        $this->settings = array_merge (
            $this->settings,
            SNTH_FW_Settings()->get_settings()['cmb2']
        );

        if($this->settings['use_prefix']) {

            if ($prefix) {
                $this->prefix = $prefix . '_';
            } else {
                $this->prefix = $this->settings['prefix'] . '_';
            }

        }

        $this->atts = $atts;
        $this->fields = $fields;

        add_action( 'cmb2_admin_init', array($this, 'register_metabox' ));
    }

    private function add_fields()
    {
        foreach ($this->fields as $field) {
            $field['id'] = $this->prefix . $field['id'];

            if ($this->settings['hide_fields']) {
                $field['id'] = '_' . $field['id'];
            }

            if ('group' == $field['type']) {
                $group_fields = $field['group_fields'];
                unset($field['group_fields']);
            }

            $this->metabox->add_field($field);

            if('group' == $field['type']) {
                $this->add_group_fields($field['id'], $group_fields);
            }
        }
    }

    /**
     * @param $group_id
     * @param $group_fields
     * @link https://github.com/WebDevStudios/CMB2/wiki/Field-Types#group
     */
    private function add_group_fields($group_id, $group_fields)
    {
        foreach ($group_fields as $field) {
            $this->metabox->add_group_field($group_id, $field);
        }
    }

    public function register_metabox()
    {
        if (function_exists('new_cmb2_box')) {
            $this->metabox = new_cmb2_box($this->atts);
        }

        $this->add_fields();
    }
}