<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 05.10.16
 * Time: 19:19
 */

class SNTH_FW_Tax
{
    /**
     * @var string
     */
    private $slug;

    /**
     * @var array
     */
    private $object_types;

    /**
     * @var array
     */
    private $args = array();

    public function __construct($slug, $object_types, $args = false)
    {
        $this->slug = $slug;
        $this->object_types = $object_types;
        if ($args) {
            array_merge($this->args, $args);
        }
        $this->set_default_args();
    }

    public function init()
    {
        add_action('init', array ($this, 'register_tax'));
    }

    public function register_tax()
    {
        register_taxonomy($this->slug, $this->object_types, $this->args);
    }

    private function set_default_args()
    {
        if (!$this->args['labels']['name']) {
            $this->args['labels']['name'] = ucwords (str_replace('_', ' ', $this->slug));
        }

        if (!$this->args['hierarchical']) {
            $this->args['hierarchical'] = true;
        }
    }
}