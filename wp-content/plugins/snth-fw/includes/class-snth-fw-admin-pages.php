<?php

class SNTH_FW_Admin_Pages
{
    private $settings = array();
    private $page_title;
    private $menu_title;
    private $capability;
    private $menu_slug;
    private $args;

    /**
     * SNTH_FW_Admin_Pages constructor.
     */
    public function __construct($page_title, $menu_title, $capability, $menu_slug, $args = false)
    {
        $this->page_title = $page_title;
        $this->menu_title = $menu_title;
        $this->capability = $capability;
        $this->menu_slug = $menu_slug;
        $this->args = $args;

        $this->settings = array_merge (
            $this->settings,
            SNTH_FW_Settings()->get_settings()['cmb2']
        );
    }

    /**
     *
     */
    public function init()
    {
        add_action('admin_menu', array ($this, 'add_menu_page'));
    }

    public function add_menu_page()
    {
        if (!$this->args['function']) {
            $this->args['function'] = array($this, 'render_menu_page');
        }

        add_menu_page(
            $this->page_title,
            $this->menu_title,
            $this->capability,
            $this->menu_slug,
            $this->args['function'],
            $this->args['icon_url'],
            $this->args['position']
        );
    }

    public function render_menu_page()
    {
        SNTH_FW_Templates::get_template('admin-pages/' . $this->menu_slug . '.php', false, SNTH_FW_VIEWS);
        //echo "<h2>Hello!</h2>";
    }
}