<?php

class SNTH_FW_User
{
    private $user_data;
    private $is_admin = false;

    public function __construct($user = false)
    {
        if($user) {
            $this->set_user_data($user);
        } else {
            $this->set_current_user_data();
        }
    }

    private function set_user_data($user)
    {
        if(is_numeric($user)) {

        }
    }

    private function set_current_user_data()
    {
        if (!is_user_logged_in ()) {
            $this->user_data = false;
        } else {
            $user = wp_get_current_user();

            $this->user_data['id'] = get_current_user_id();
            $this->user_data['login'] = $user->user_login;
            $roles = get_userdata($this->user_data['id'])->roles;
            $this->user_data['is_admin'] = (in_array("administrator", $roles)) ? true : false;
            $this->user_data['roles'] = get_userdata($this->user_data['id'])->roles;
            $this->user_data['first_name'] = $user->user_firstname;
            $this->user_data['last_name'] = $user->user_lastname;
            $this->user_data['full_name'] = $this->user_data['first_name'] . " " . $this->user_data['last_name'];
            $this->user_data['nicename'] = $user->user_nicename;
        }
    }

    public function get_user_data()
    {
        return $this->user_data;
    }

    public function is_admin()
    {
        return $this->user_data['is_admin'];
    }
}