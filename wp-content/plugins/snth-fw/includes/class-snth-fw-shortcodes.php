<?php

class SNTH_FW_Shortcodes
{
    private $shortcode;
    private $template_name;
    private $args;

    /**
     * SNTH_FW_Shortcodes constructor.
     *
     * @param $shortcode
     * @param bool $template_name
     * @param array $args default values of shortcodes atts
     * @param string $callback
     */
    public function __construct($shortcode, $template_name = false, $args = array(), $callback = 'false')
    {
        $this->shortcode = $shortcode;
        $this->template_name = $template_name;
        $this->args = $args;

        if ('false' !== $callback) {
            add_shortcode($shortcode, $callback());
        } else {
            add_shortcode($shortcode, array($this, 'add_shortcode'));
        }
    }

    public function add_shortcode($atts)
    {
        if (!empty($this->args) && is_array($this->args)) {
            $atts = shortcode_atts(
                $this->args, $atts, $this->shortcode
            );
        } else {
            $atts = array();
        }

        $template_name = SNTH_FW_Settings()->get_settings()['shortcodes_dir'] . '/';
        $template_name .= ($this->template_name) ? $this->template_name : str_replace( '_', '-', $this->shortcode);
        $template_name .= '.php';

        return SNTH_FW_Templates::get_template_html($template_name, $atts);
    }
}