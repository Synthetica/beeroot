<?php

class SNTH_FW_Core
{
    protected $settings = array();
    /**
     * The single instance of the class.
     *
     * @var SNTH_FW_Core
     */
    protected static $_instance = null;

    /**
     * Main SNTH_FW_Core Instance.
     *
     * @return SNTH_FW_Core - Main instance.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * SNTH_FW_Core Constructor.
     * @since 0.0.1
     */
    private function __construct()
    {
        $this->settings = array_merge (
            $this->settings,
            SNTH_FW_Settings()->get_settings()['general']
        );

        add_action('wp_enqueue_scripts', array($this, 'enqueue_styles'));
        add_action('wp_enqueue_scripts', array($this, 'enqueue_scripts'));
    }

    /**
     * Enqueueing styles
     */
    public function enqueue_styles()
    {
//        wp_register_style('main', MNB_CSS . '/main.css');
//        wp_enqueue_style('main');
    }

    /**
     * Enqueueing scripts
     */
    public function enqueue_scripts()
    {
//        wp_register_script('main', MNB_JS . '/main.js', array('jquery', 'validation'), MNB_VERSION, true);
//        wp_enqueue_script('main');
//
//        wp_localize_script('main', 'MananaBB', array('ajaxurl' => admin_url('admin-ajax.php')));
    }
}

/**
 * Main instance of SNTH_FW_Core.
 * @return SNTH_FW_Core
 */
function SNTH_FW_Core() {
    return SNTH_FW_Core::instance();
}

// Global for backwards compatibility.
$GLOBALS['snth_fw_core'] = SNTH_FW_Core();