<?php

class SNTH_FW_Email
{
    private $settings = array();
    private $to = null;
    private $subject = null;
    private $args = null;
    private $template = null;

    /**
     * SNTH_FW_Email constructor.
     * @param string/array $to
     * @param string $subject
     * @param array $args
     *  01 - template, default general
     *  02 -
     */
    public function __construct($to, $subject, $args = array())
    {
        $this->settings = array_merge(
            $this->settings,
            SNTH_FW_Settings()->get_settings()['general'],
            SNTH_FW_Settings()->get_settings()['email']
        );

        // Headers
        if ($args['headers']) {
            $this->settings['headers'] = $args['headers'];
        }

        // header footer template
        if ($args['template']) {
            $this->settings['template'] = $args['template'];
        }

        $this->template = $this->settings['path'] . '/' . $this->settings['template'];

        $this->to = $to;
        $this->subject = $subject;
    }

    /**
     * @param $body
     * @param array $args
     */
    public function send_email($body, $args = array())
    {
        $message = $this->get_email_body($body, $args);

        return wp_mail($this->to, $this->subject, $message, $this->settings['headers']);
    }

    public function get_email_body($body, $args)
    {
        $template  = '';
        $template .= $this->get_email_header();
        $template .= $this->get_email_content($body, $args);
        $template .= $this->get_email_footer();

        return $template;
    }

    public function get_email_header()
    {
        return SNTH_FW_Templates::get_template_html($this->template . '/header.php');
    }

    public function get_email_content($body, $args)
    {
        return SNTH_FW_Templates::get_template_html($this->settings['path'] . '/' . $body . '.php', $args);
    }

    public function get_email_footer()
    {
        return SNTH_FW_Templates::get_template_html($this->template . '/footer.php');
    }

    public function send_notification()
    {

    }
}