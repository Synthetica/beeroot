<?php

class SNTH_FW_Templates
{
    /**
     * MNB_Core Constructor.
     * @since 0.0.1
     */
    private function __construct()
    {
        $this->settings = array_merge (
            $this->settings,
            SNTH_FW_Settings()->get_settings()['general']
        );
    }

    /**
     * Get templates passing attributes and including the file.
     *
     * @param string $template_name
     * @param array $args
     * @param string $template_path
     */
    public static function get_template($template_name, $args = array(), $template_path = '', $default_path = '')
    {
        if (!empty($args) && is_array($args)) {
            extract($args);
        }

        $located = self::locate_template($template_name, $template_path, $default_path);

        if (!file_exists($located)) {
            return;
        }

        $located = apply_filters('snth_fw_get_template', $located, $template_name, $args, $template_path);

        do_action('snth_fw_before_template_part', $template_name, $template_path, $located, $args);

        include($located);

        do_action('snth_fw_after_template_part', $template_name, $template_path, $located, $args);
    }

    /**
     * Like get_template, but returns the HTML instead of outputting.
     *
     * @param $template_name
     * @param array $args
     * @param string $template_path
     * @return string
     */
    public static function get_template_html($template_name, $args = array(), $template_path = '', $default_path = '')
    {
        ob_start();
        self::get_template($template_name, $args, $template_path);
        return ob_get_clean();
    }

    /**
     * Locate a template and return the path for inclusion.
     *
     * @param $template_name
     * @param string $template_path
     * @return string
     */
    public static function locate_template($template_name, $template_path = '', $default_path = '')
    {
        if (!$template_path) {
            $template_path = SNTH_FW_Settings()->get_settings()['templates_path'];
        }

        $template = locate_template(
            array(
                trailingslashit($template_path) . $template_name,
                $template_name
            )
        );

        if(!$template) {
            $template = trailingslashit($template_path) . $template_name;
        }

        return apply_filters('snth_fw_locate_template', $template, $template_name, $template_path);
    }

}