<?php
/**
 * Created by PhpStorm.
 * User: vladislav
 * Date: 05.10.16
 * Time: 17:13
 */

class SNTH_FW_CPT
{
    private $slug;
    private $args = array();


    /**
     * SNTH_FW_CPT constructor.
     *
     * @param $slug
     * @param bool $label Plural title of CPT item
     */
    public function __construct($slug, $label, $args = false)
    {
        $this->slug = $slug;
        $this->args['label'] = $label;
        if ($args) {
            array_merge($this->args, $args);
        }
        $this->set_default_args();
    }

    public function init()
    {
        add_action('init', array ($this, 'register_cpt'));
    }

    public function register_cpt()
    {
        register_post_type($this->slug, $this->args);
    }

    private function set_default_args()
    {
        if (!$this->args['public']) {
            $this->args['public'] = true;
        }

        if (!$this->args['supports']) {
            $this->args['supports'] = array(
                'title', 'editor', 'excerpt', 'thumbnail'
            );
        }
    }
}