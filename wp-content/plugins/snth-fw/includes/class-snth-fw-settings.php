<?php

class SNTH_FW_Settings
{
    /**
     * The single instance of the class.
     *
     * @var SNTH_FW_Settings
     */
    protected static $_instance = null;

    /**
     * Settings for theme
     * @var array
     */
    protected static $_settings = array();

    /**
     * Main SNTH_FW_Settings Instance.
     *
     * @since 0.0.1
     * @static
     * @return SNTH_FW_Settings - Main instance.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * MNB_Settings Constructor.
     * @since 0.0.1
     */
    private function __construct()
    {
        $this->set_default_settings();
    }

    /**
     * Set default plugin settings
     */
    private function set_default_settings()
    {
        // General settings
        self::$_settings['general'] = array (
            'admin_emails' => array (
                'syntheticafreon@gmail.com',
                'i.synthetica@gmail.com'
            ),
        );

        self::$_settings['cmb2'] = array(
            'use_prefix' => true,
            'prefix' => 'snth_fw',
            'hide_fields' => true
        );

        self::$_settings['admin'] = array(
            'hide_color_scheme' => true,
        );

        self::$_settings['email'] = array(
            'path' => 'email',
            'template' => 'general',
            'headers' => array(
                'From: Manana Baby <mail@mananababy.com>',
                'content-type: text/html'
            )
        );

        self::$_settings['templates_path'] = get_stylesheet_directory() . '/templates';
        self::$_settings['shortcodes_dir'] = 'shortcodes';
    }

    /**
     * Get theme settings
     *
     * @return array
     */
    public function get_settings()
    {
        return apply_filters('snth_fw_get_settings', self::$_settings);
    }


}

/**
 * Main instance of SNTH_FW_Settings.
 * @return SNTH_FW_Settings
 */
function SNTH_FW_Settings() {
    return SNTH_FW_Settings::instance();
}

// Global for backwards compatibility.
$GLOBALS['snth_fw_settings'] = SNTH_FW_Settings();
