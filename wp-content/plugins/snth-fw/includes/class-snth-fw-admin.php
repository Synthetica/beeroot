<?php

class SNTH_FW_Admin
{
    private $settings = array();

    /**
     * The single instance of the class.
     *
     * @var SNTH_FW_Admin
     */
    protected static $_instance = null;

    /**
     * Main SNTH_FW_Admin Instance.
     *
     * @return SNTH_FW_Admin - Main instance.
     */
    public static function instance()
    {
        if ( is_null( self::$_instance ) ) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * SNTH_FW_Admin Constructor.
     * @since 0.0.1
     */
    private function __construct()
    {
        $this->settings = array_merge (
            $this->settings,
            SNTH_FW_Settings()->get_settings()['admin']
        );

        if($this->settings['hide_color_scheme']) {
            remove_action( 'admin_color_scheme_picker', 'admin_color_scheme_picker' );
        }
    }
}

/**
 * Main instance of SNTH_FW_Admin.
 * @return SNTH_FW_Admin
 */
function SNTH_FW_Admin() {
    return SNTH_FW_Admin::instance();
}

// Global for backwards compatibility.
$GLOBALS['snth_fw_admin'] = SNTH_FW_Admin();