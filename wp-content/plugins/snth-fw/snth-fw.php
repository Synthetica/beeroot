<?php
/**
 * Plugin Name: Synthetica Framework
 * Plugin URI: https://synthetica.com.ua/
 * Description: A developer toolkit that helps to speedup wordpress developing.
 * Version: 1.0.0
 * Author: Vlad Synthetica Lisnyi
 * Author URI: https://synthetica.com.ua/
 *
 * Text Domain: snth-fw
 * Domain Path: /i18n/languages/
 *
 * @package SNTH Framework
 * @category Core
 * @author Vlad Synthetica Lisnyi
 */
if (! defined( 'ABSPATH')) {
    exit; // Exit if accessed directly.
}

if (! class_exists('SNTH_FW_Controller')) {
    class SNTH_FW_Controller
    {
        /**
         * The single instance of the class.
         *
         * @var SNTH_FW_Controller
         */
        protected static $_instance = null;

        protected static $version = '1.0.0';

        /**
         * Main SNTH_FW_Controller Instance.
         *
         * @return SNTH_FW_Controller - Main instance.
         */
        public static function instance()
        {
            if (is_null(self::$_instance)) {
                self::$_instance = new self();
            }
            return self::$_instance;
        }

        /**
         * SNTH_FW_Controller Constructor.
         * @since 0.0.1
         */
        private function __construct()
        {
            $this->define_constants();

            spl_autoload_register(array($this, 'autoload'));

            $this->includes();

            if ( is_admin() ) {
                $this->admin_includes();
            }
        }

        public function define_constants()
        {
            define ('SNTH_FW_VER', self::$version);
            define ('SNTH_FW_PATH', untrailingslashit(plugin_dir_path( __FILE__ )));
            define ('SNTH_FW_INC', SNTH_FW_PATH . '/includes');
            define ('SNTH_FW_VIEWS', SNTH_FW_PATH . '/templates');
            define ('SNTH_FW_URL', untrailingslashit(plugin_dir_url( __FILE__ )));
            define ('SNTH_FW_ASSETS', SNTH_FW_URL . '/assets');
            define ('SNTH_FW_CSS', SNTH_FW_ASSETS . '/css');
            define ('SNTH_FW_JS', SNTH_FW_ASSETS . '/js');
            define ('SNTH_FW_IMG', SNTH_FW_ASSETS . '/img');
            define ('SNTH_FW_VENDORS', SNTH_FW_ASSETS . '/vendors');
        }

        /**
         * Framework class autoloader
         *
         * @param $class
         */
        public function autoload($class_name)
        {
            if (class_exists($class_name)) {
                return;
            }

            $class_path = SNTH_FW_PATH . DIRECTORY_SEPARATOR . 'includes'
                . DIRECTORY_SEPARATOR . 'class-' . strtolower(str_replace( '_', '-', $class_name )) . '.php';

            if ( file_exists( $class_path ) ) {
                include $class_path;
            }
        }

        /**
         * Includes framework files
         */
        public function includes()
        {
            include_once SNTH_FW_INC . '/class-snth-fw-settings.php';
            include_once SNTH_FW_INC . '/class-snth-fw-core.php';

            if (file_exists(SNTH_FW_INC . '/cmb2/init.php')) {
                require_once  SNTH_FW_INC . '/cmb2/init.php';
            }
        }

        /**
         * Includes admin framework files
         */
        public function admin_includes()
        {
            include_once SNTH_FW_INC . '/class-snth-fw-admin.php';
        }
    }

    /**
     * Main instance of SNTH_FW_Controller.
     * @return SNTH_FW_Controller
     */
    function SNTH_FW_Controller() {
        return SNTH_FW_Controller::instance();
    }

    // Global for backwards compatibility.
    $GLOBALS['snth_fw_controller'] = SNTH_FW_Controller();
}


