<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'synthetica_beetroot');

/** MySQL database username */
define('DB_USER', 'Synthetica');

/** MySQL database password */
define('DB_PASSWORD', 'V@ld1s81');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Ycq7{#dB:@^a^J}}_ ++V@V$q0`RW!by$*sU{gBxffR{|uG<6J0|L%WQbYjV 5&l');
define('SECURE_AUTH_KEY',  'b>[?cF]k+<6{d&W+@&,lGA+[$T>*pS/BEfQyeg:VPpNptZvTvT6}|@*1v,Gd_G*:');
define('LOGGED_IN_KEY',    'q/Oic $1`Pa}IeGP8>{q`Hqfh(wB|X--NtQf94P`*pJg|y,W`f)bl}W<fa&sdS1d');
define('NONCE_KEY',        'E8TE~^wUPhdW6qQ|=ag0ikF)C5ST5pe&=Z/k%Xm+Jm6z@+2!%C8^|HVA+YA@Pkw#');
define('AUTH_SALT',        '!O>e@68S5J$6iGNp.-NSi-ZiyEmY}MubYYd2mI>-zWQ;LQ`:k0vMgC?6*NraaX-$');
define('SECURE_AUTH_SALT', 'doAx<+~h|)u5Z>t[!|eNJ;`4N+l17j*V67{DtX)%Y0uE_uW4k@ncu_~kKG]s40s[');
define('LOGGED_IN_SALT',   'c-en:e>#*+`ZbHO9E!L|[=Ru6a{JALhJD2+Thp;O:-(c`~YF/xno8QMaqx-L9-,!');
define('NONCE_SALT',       '|f3smN*z0bBaBR1+M,Clmrvt:$ySIj]SB4Zpsw]}1%a-A*)&y|YVg9_.IFjleje:');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

define('WPCF7_AUTOP', false );

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
